package com.deyneka.tools;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

/**
 * Reentrant implementation of the <tt>EntityLocker</tt> interface.
 * An instance of <tt>ReentrantEntityLocker</tt> has parameter: cleaning frequency.
 * The cleaning frequency is the number of executions before attempt of cleaning unused locks.
 *
 * @param <E> the type of entity ids
 * @author Aleksandr Deyneka
 */
public class ReentrantEntityLocker<E> implements EntityLocker<E> {
    private final Map<E, ReentrantLock> lockMap = new ConcurrentHashMap<>();
    private final ReadWriteLock globalLock = new ReentrantReadWriteLock();
    private final AtomicInteger unlockCounter = new AtomicInteger(0);
    private static final int DEFAULT_CLEANING_FREQUENCY = 3072;
    private int cleaningFrequency;

    /**
     * Constructs <tt>ReentrantEntityLocker</tt> with the specified cleaning frequency.
     *
     * @param cleaningFrequency the frequency of cleaning inner data
     * @throws IllegalArgumentException if the cleaningFrequency is negative or 0
     */
    public ReentrantEntityLocker(int cleaningFrequency) {
        if (cleaningFrequency <= 0)
            throw new IllegalArgumentException("Illegal cleaning frequency: " + cleaningFrequency);
        this.cleaningFrequency = cleaningFrequency;
    }

    /**
     * Constructs <tt>ReentrantEntityLocker</tt> with default frequency of cleaning (3072).
     */
    public ReentrantEntityLocker() {
        this(DEFAULT_CLEANING_FREQUENCY);
    }

    @Override
    public void execute(E entityId, Runnable task) {
        throwExceptionIfNull(entityId, "entityId must be not null");
        throwExceptionIfNull(task, "task must be not null");
        globalLock.readLock().lock();
        try {
            lock(entityId);
            try {
                task.run();
            } finally {
                unlock(entityId);
            }
        } finally {
            globalLock.readLock().unlock();
        }
    }

    private void lock(E entityId) {
        ReentrantLock curLock = null;
        do {
            if (curLock != null)
                curLock.unlock();
            curLock = lockMap.computeIfAbsent(entityId, e -> new ReentrantLock(true));
            curLock.lock();
        } while (lockMap.get(entityId) != curLock);
    }

    private void unlock(E entityId) {
        Optional.ofNullable(lockMap.get(entityId)).ifPresent(lock -> {
            if (lock.isHeldByCurrentThread())
                lock.unlock();
        });
        clearUnusedLocksIfNeeded();
    }

    @Override
    public void execute(List<E> entityIds, Comparator<E> entityIdsComparator, Runnable task) {
        throwExceptionIfNull(entityIds, "entityIds must be not null");
        throwExceptionIfNull(entityIdsComparator, "entityIdsComparator must be not null");
        throwExceptionIfNull(task, "task must be not null");
        globalLock.readLock().lock();
        try {
            List<E> sortedList = entityIds.stream()
                    .distinct()
                    .sorted(entityIdsComparator)
                    .collect(Collectors.toList());
            List<E> lockedList = new ArrayList<>();
            try {
                sortedList.forEach(entityId -> {
                    lock(entityId);
                    lockedList.add(entityId);
                });
                task.run();
            } finally {
                lockedList.stream()
                        .sorted(entityIdsComparator.reversed())
                        .forEach(this::unlock);
            }
        } finally {
            globalLock.readLock().unlock();
        }
    }

    private void throwExceptionIfNull(Object object, String exceptionMessage) {
        if (object == null)
            throw new IllegalArgumentException(exceptionMessage);
    }

    @Override
    public boolean tryLockAndExecute(E entityId, Runnable task, long timeout, TimeUnit unit) throws InterruptedException {
        long startTime = System.nanoTime();
        throwExceptionIfNull(entityId, "entityId must be not null");
        throwExceptionIfNull(task, "task must be not null");
        throwExceptionIfNull(unit, "unit must be not null");
        globalLock.readLock().lock();
        long timeoutLeft = unit.convert(timeout, TimeUnit.NANOSECONDS) - (System.nanoTime() - startTime);
        try {
            if (timeoutLeft > 0 && tryLock(entityId, timeoutLeft)) {
                try {
                    task.run();
                    return true;
                } finally {
                    unlock(entityId);
                }
            } else {
                return false;
            }
        } finally {
            globalLock.readLock().unlock();
        }
    }

    private boolean tryLock(E entityId, long timeout) throws InterruptedException {
        ReentrantLock curLock;
        boolean result;
        do {
            long startTime = System.nanoTime();
            curLock = lockMap.computeIfAbsent(entityId, e -> new ReentrantLock(true));
            timeout = timeout - (System.nanoTime() - startTime);
        } while ((result = curLock.tryLock(timeout, TimeUnit.NANOSECONDS)) && lockMap.get(entityId) != curLock);//for cleaning
        return result;
    }

    @Override
    public boolean tryLockAndExecute(List<E> entityIds, Comparator<E> entityIdsComparator, Runnable task, long timeout, TimeUnit unit) throws InterruptedException {
        long startTime = System.nanoTime();
        throwExceptionIfNull(entityIds, "entityIds must be not null");
        throwExceptionIfNull(entityIdsComparator, "entityIdsComparator must be not null");
        throwExceptionIfNull(task, "task must be not null");
        throwExceptionIfNull(unit, "unit must be not null");
        globalLock.readLock().lock();
        try {
            List<E> sortedList = entityIds.stream()
                    .distinct()
                    .sorted(entityIdsComparator)
                    .collect(Collectors.toList());
            List<E> lockedList = new ArrayList<>();
            long timeoutLeft = unit.convert(timeout, TimeUnit.NANOSECONDS) - (System.nanoTime() - startTime);
            try {
                for (E entityId : sortedList) {
                    long beforeLockTime = System.nanoTime();
                    if (timeoutLeft > 0 && tryLock(entityId, timeoutLeft)) {
                        lockedList.add(entityId);
                    } else {
                        return false;
                    }
                    long afterLockTime = System.nanoTime();
                    timeoutLeft = timeoutLeft - (afterLockTime - beforeLockTime);
                }
                task.run();
                return true;
            } finally {
                lockedList.stream()
                        .sorted(entityIdsComparator.reversed())
                        .forEach(this::unlock);
            }
        } finally {
            globalLock.readLock().unlock();
        }
    }

    @Override
    public void execute(Runnable task) {
        throwExceptionIfNull(task, "task must be not null");
        globalLock.writeLock().lock();
        try {
            task.run();
        } finally {
            globalLock.writeLock().unlock();
        }
    }

    @Override
    public boolean tryLockAndExecute(Runnable task, long timeout, TimeUnit unit) throws InterruptedException {
        throwExceptionIfNull(task, "task must be not null");
        throwExceptionIfNull(unit, "unit must be not null");
        if (globalLock.writeLock().tryLock(timeout, unit)) {
            try {
                task.run();
                return true;
            } finally {
                globalLock.writeLock().unlock();
            }
        } else {
            return false;
        }
    }

    private void clearUnusedLocksIfNeeded() {
        if (unlockCounter.incrementAndGet() >= cleaningFrequency) {
            unlockCounter.set(0);
            clearNotLockedEntities();
        }
    }

    private void clearNotLockedEntities() {
        lockMap.values().removeIf(lock -> !lock.isLocked() && !lock.hasQueuedThreads() && !lock.isHeldByCurrentThread());
    }

    @Override
    public void clear() {
        globalLock.writeLock().lock();
        lockMap.clear();
        globalLock.writeLock().unlock();
    }
}
